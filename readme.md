# Dom-safe
The set of functions for safe working with browser DOM 

##How to install
```bash
    npm install dom-safe
```

##Usage

```javascript
    import dom from '/node_modules/dom-safe/main.js'
```

### clearChilds() 
Safe remove all childs from DOM node with removing all listeners

###class(classObj)
convert object fields to DOM class.
The keys of objects becomes classes in DOM class string depends on
theirs values. The keys with false values will be no added to resulting className

```javascript
    const cstr = dom.class({ one: true, two: false, three: true });
    console.log(cstr); //'one three';
```
